const express = require('express')
const csv = require('csv-parser')
const app = express()
const fs = require('fs');
const linebot = require('linebot');// 判別開發環境
if (process.env.NODE_ENV !== 'production') {      // 如果不是 production 模式
 require('dotenv').config()                      // 使用 dotenv 讀取 .env 檔案
}
const line = require('@line/bot-sdk');
var getJSON = require('get-json');
const { DH_NOT_SUITABLE_GENERATOR } = require('constants');

const bot = linebot({
 channelId: process.env.CHANNEL_ID,
 channelSecret: process.env.CHANNEL_SECRET,
 channelAccessToken: process.env.CHANNEL_ACCESS_TOKEN
});

const client = new line.Client({
  channelAccessToken: process.env.CHANNEL_SECRET,
  channelSecret: process.env.CHANNEL_ACCESS_TOKEN
});

const linebotParser = bot.parser();

_getCSV();
var vaccine=[];

const results = [];
function _getCSV(){
  // fs.createReadStream('food.csv')
  // .pipe(csv())
  // .on('data', (data) => results.push(data))
  // .on('end', () => {
  //   console.log(results[0]);
  //   console.log(results[1]['']);
  //   console.log(results[2]);
  //   console.log(results[3]);
  //   // console.log(results[4]);
  // });
}
function _getJSON(place) {
    if(place=="child"){
      fs.readFile('child.json', 'utf8', (err, data) => {
        var data = JSON.parse(data);
        vaccine = data;
        console.log("testa");
        console.log(data[0]);
        
    });
    }
//     //clearTimeout(timer);
    
//     // getJSON('https://data.epa.gov.tw/api/v1/aqx_p_432?limit=1000&api_key=9be7b239-557b-4c10-9775-78cadfc555e9&format=json', function(error, response) {
//     //   //response=JSON.parse(response);
//     //   // response.map(function(e, i) {
//     //   //   pm[i] = [];
//     //   //   pm[i][0] = e.SiteName;
//     //   //   pm[i][1] = e['PM2.5'] * 1;
//     //   //   pm[i][2] = e.PM10 * 1;
//     //   // });
      
      
//     //   array = response.records;
      
//     // });
//     //timer = setInterval(_getJSON, 1800000); //每半小時抓取一次新資料
  }
bot.on('message', function (event) {
  console.log("event");
  console.log(event);
  console.log("end event");
  var id = event.source.userId;
  var this_message = event.message.text;
  
  if (!fs.existsSync('message.json')) {
    fs.writeFileSync('message.json', '');
  }
  fs.readFile('message.json', 'utf8', (err, data) => {
    
    if(this_message=='location'){
      event.reply({
        type: 'image',
        originalContentUrl: 'https://i.imgur.com/dXScwS2.png',
        previewImageUrl: 'https://i.imgur.com/dXScwS2.png'
      });
    }
    if(this_message=='clear'){
      let message  = [];
      fs.writeFile('message.json', JSON.stringify(message), (err) => {
        if (err) reject(err);
      });
    }
    else{
      if (err) reject(err);
      let message  = data ? JSON.parse(data) : [];
      message.push(this_message);
      console.log(message);
      var length = message.length;

      if(message[length-2]=="search"){
        console.log("search");
        fs.readFile('clinic.json', 'utf8', (err, data) => {
            var  clinic = JSON.parse(data);
            var temp=[];
            for(var i=0;i<clinic.length;i++){
              temp[i].type='location';
              temp[i].title='my location';
              temp[i].address=clinic[i]['地址'];
              temp[i].latitude = clinic[i]['latitude'];
              temp[i].longitude=clinic[i]['longitude'];
            }
            
        })

        event.reply({
          type: 'location',
          title: 'my location',
          address: "302台灣新竹縣竹北市嘉豐南路一段30號1樓",
          latitude: 24.81188096127696,
          longitude: 121.03455832940254
      });
        console.log("end reply");
      }
      if(this_message=="search"){
        event.reply("what");
      }
      if(this_message=="vaccine"){
        event.reply("kind");
      }
      if(this_message=="search dangerous place"){
        event.reply("enter the place you want");
      }
      if(this_message=="search available ward"){
        event.reply("enter the hospital");
      }


      if(message[length-2]=="vaccine"){
        _getJSON(this_message);
        event.reply("enter your place");
      }
      if(message[length-3]=="vaccine" ){
       
        var place=[];
        for(var i=0;i<vaccine.length;i++){
          console.log(vaccine[i]['地址'][0]+vaccine[i]['地址'][1]+vaccine[i]['地址'][2]);
          if(vaccine[i]['地址'][0]==this_message[0] && vaccine[i]['地址'][1]==this_message[1] && vaccine[i]['地址'][2]==this_message[2]){
            console.log("get");
            place.push(vaccine[i]['合約醫療院所名稱']);
          }
        }
        console.log(place.length);
        event.reply(
          place[0]+
          ',\n'+
          place[1]+
          ',\n'+
          place[2]+
          ',\n'+
          place[3]
        );
      }
      if(message[length-2]=="search dangerous place"){
        fs.readFile('danger.json', 'utf8', (err, data) => {
          var danger = JSON.parse(data);
          if(danger[this_message]){
            event.reply(danger[this_message]);
          }
          else{
            event.reply("no information, maybe it's safe");
          }
          
        });
      }
      if(message[length-2]=="search available ward"){
        fs.readFile('freebed.json', 'utf8', (err, data) => {
          var hospital = JSON.parse(data);
          if(hospital[this_message]){
            event.reply("still have "+hospital[this_message]+" freebed");
          }
          else{
            event.reply("no information. Maybe another hopital?");
          }
          
        });
      }
      

      fs.writeFile('message.json', JSON.stringify(message), (err) => {
        
        if (err) reject(err);

        
      });
    }
    
    
  });

  // event.reply(<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3105.1503819264813!2d-77.03871848493745!3d38.89767627957064!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b7b7bcdecbb1df%3A0x715969d86d0b76bf!2sThe+White+House!5e0!3m2!1sen!2sin!4v1511332214641" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>);
    // console.log("fuck");
    // console.log(array[1]);
    // console.log(event);
    // console.log(`使用者 ID: ${event.source.userId}`);
    // for(let i=0;i<array.length;i = i+1){
    //   console.log(array[i].SiteName);
    //   if(event.message.text==array[i].SiteName){
    //     event.reply(array[i]["SiteName"]+"的pm2.5是 "+array[i]["PM2.5"]);
    //     // event.reply("test");
    //   }
    // }

});
app.post('/', linebotParser);
app.listen(process.env.PORT || 3000, () => {
 console.log('Express server start')
});
// var linebot = require('linebot');
// var express = require('express');
// var getJSON = require('get-json');

// var bot = linebot({
//   channelId: 'channelId',
//   channelSecret: 'channelSecret',
//   channelAccessToken: 'channelAccessToken'
// });

// var timer;
// var pm = [];
// _getJSON();

// _bot();
// const app = express();
// const linebotParser = bot.parser();
// app.post('/', linebotParser);

// //因為 express 預設走 port 3000，而 heroku 上預設卻不是，要透過下列程式轉換
// var server = app.listen(process.env.PORT || 8080, function() {
//   var port = server.address().port;
//   console.log("App now running on port", port);
// });

// function _bot() {
//   bot.on('message', function(event) {
//     if (event.message.type == 'text') {
//       var msg = event.message.text;
//       var replyMsg = '';
//       if (msg.indexOf('PM2.5') != -1) {
//         pm.forEach(function(e, i) {
//           if (msg.indexOf(e[0]) != -1) {
//             replyMsg = e[0] + '的 PM2.5 數值為 ' + e[1];
//           }
//         });
//         if (replyMsg == '') {
//           replyMsg = '請輸入正確的地點';
//         }
//       }
//       if (replyMsg == '') {
//         replyMsg = '不知道「'+msg+'」是什麼意思 :p';
//       }

//       event.reply(replyMsg).then(function(data) {
//         console.log(replyMsg);
//       }).catch(function(error) {
//         console.log('error');
//       });
//     }
//   });

// }

// function _getJSON() {
//   clearTimeout(timer);
//   getJSON('https://data.epa.gov.tw/api/v1/aqx_p_432?limit=1000&api_key=9be7b239-557b-4c10-9775-78cadfc555e9&format=json', function(error, response) {
//     //response=JSON.parse(response);
//     response.map(function(e, i) {
//       pm[i] = [];
//       pm[i][0] = e.SiteName;
//       pm[i][1] = e['PM2.5'] * 1;
//       pm[i][2] = e.PM10 * 1;
//     });
//   });
//   timer = setInterval(_getJSON, 1800000); //每半小時抓取一次新資料
// }